# react-ui-tree

Fork of https://github.com/pqx/react-ui-tree

React tree component

This project was initially developed for a webpage builder. It maintains an
internal tree structure within the component through
[js-tree](https://github.com/wangzuo/js-tree).

### Usage

```javascript
<Tree
  paddingLeft={20}              // left padding for children nodes in pixels
  tree={this.state.tree}        // tree object
  onChange={this.handleChange}  // onChange(tree) tree object changed
  renderNode={this.renderNode}  // renderNode(node) return react element
/>

// a sample tree object
// node.children, node.collapsed, node.leaf properties are hardcoded
{
  "module": "react-ui-tree",
  "children": [{
    "collapsed": true,
    "module": "dist",
    "children": [{
      "module": "node.js"
    }]
  }]
}
```

### Development

* npm install
* npm start
* http://<hostIP>:8080/example

### License

MIT
